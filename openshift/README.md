OpenShift-Specific Objects
==========================

This directory includes OpenShift/OKD-specific objects, including an imageStream and buildConfig for building an image based on the contents of this repository.

*Note:* You may not need to build this yourself, if you want to use the OIT-supplied image instead.

## Files

*   imageStream.yaml - creates an imageStream within your project, to store completed Shibboleth container images
*   buildconfig.yaml - a buildConfig for building the duke-shibboleth-container image from the contents of this repo
